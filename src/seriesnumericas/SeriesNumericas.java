/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriesnumericas;

import java.util.Scanner;

/**
 *
 * @author kondor
 */
public class SeriesNumericas {

    public static void main(String[] args) {
        String opcionMenu;
        int metodo = 0;
        int xi;
        Scanner sc = new Scanner(System.in);
        padovan pv = new padovan();
        pascal pc = new pascal();
        fibonacci fn = new fibonacci();
        String cadena;
        menu();

        while (true) {
            boolean salida = false;
            opcionMenu = sc.nextLine();

            if (opcionMenu.equals("1")) {
                metodo = 1;
                System.out.println("");
                System.out.print("**Has seleccionado la opción: FIBONACCI**\nIntroduce limite de Fibonacci\n");
                fn.fibonacci(sc.nextInt());
                System.out.println("\nOTRO LIMITE(1)\n        MENU(2)\n        SALIR(?) <--- cualquier numero diferente de 1 o 2");

            } else if (opcionMenu.equals("2")) {
                metodo = 2;
                System.out.println("");
                System.out.print("**Has seleccionado la opción: PASCAL**\nIntroduce limite de Pascal\n");
                pc.pascal(sc.nextInt());
                System.out.println("\nOTRO LIMITE(1)\n        MENU(2)\n        SALIR(?) <--- cualquier numero diferente de 1 o 2");
            } else if (opcionMenu.equals("3")) {
                metodo = 3;
                System.out.println("");
                System.out.print("**Has seleccionado la opción: PADOVAN**\nIntroduce limite de Padovan\n");
                pv.padovan(sc.nextInt());
                System.out.println("\nOTRO LIMITE(1)\n        MENU(2)\n        SALIR(?) <--- cualquier numero diferente de 1 o 2");
            } else if (opcionMenu.equals("9")) {
                System.out.println("");
                System.out.println("Programa finalizado");
                break;

            } else {
                System.out.println("");
                System.out.println("Opcion seleccionada invalida\nIngresa una opcion correcta...        salir(9)\n");
                continue;

            }
            System.out.println("");
            while (salida != true) {
                //System.out.println("\nOTRO LIMITE(1)\n        MENU(2)\n        SALIR(?) <--- cualquier numero diferente de 1 o 2");
                cadena = sc.nextLine();
                if (isNumeric(cadena)) {
                    salida = opciones(cadena, metodo);
               
                } else {
                    System.out.println("Seleccione una de las opciones:\n");
                }

            }
        }
    }

    static void menu() {
        System.out.println("Selecciona una opción");
        System.out.println("\t1 - Algoritmo de Fibonacci");
        System.out.println("\t2 - Algoritmo de Pascal");
        System.out.println("\t3 - Algoritmo de Padovan");
        System.out.println("\t9 - Salir");
    }

    static boolean opciones(String x, int metodo) {
        Scanner sc2 = new Scanner(System.in);

        boolean salida = false;

        if (x.equals("1")) {

            if (metodo == 1) {
                fibonacci fn = new fibonacci();
                System.out.println("Introduce limite de Fibonacci\n");
                fn.fibonacci(sc2.nextInt());
                

            } else if (metodo == 2) {
                pascal pc = new pascal();
                System.out.println("Introduce limite de Pascal\n");
                pc.pascal(sc2.nextInt());
            } else {
                padovan pv = new padovan();
                System.out.println("Introduce limite de Padovan\n");
                pv.padovan(sc2.nextInt());
            }
            System.out.println("\nOTRO LIMITE(1)\n        MENU(2)\n        SALIR(?) <--- cualquier numero diferente de 1 o 2");

        } else if (x.equals("2")) {
            salida = true;
            menu();
            return salida;
        } else {
            System.out.println("\nPROGRAMA FINALIZADO\n");
            System.exit(0);
        }
        
        salida = false;
        
        return salida;
        
    }

    static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

}
